package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IHashingPropertyService {

    @NotNull
    String getSaltProperty();

    @NotNull
    Integer getCounterProperty();

}
