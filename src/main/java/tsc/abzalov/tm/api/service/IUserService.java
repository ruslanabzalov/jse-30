package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

public interface IUserService extends IService<User> {

    void create(@Nullable String login, @Nullable String password, @Nullable Role role,
                @Nullable String firstName, @Nullable String lastName, @Nullable String email);

    void create(@Nullable String login, @Nullable String password,@Nullable String firstName,
                @Nullable String lastName, @Nullable String email);

    boolean isUserExist(@Nullable String login, @Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User editPasswordById(@Nullable String id, @Nullable String newPassword);

    @Nullable
    User editUserInfoById(@Nullable String id, @Nullable String firstName, @Nullable String lastName);

    void deleteByLogin(@Nullable String login);

    @Nullable
    User lockUnlockById(@Nullable String id);

    @Nullable
    User lockUnlockByLogin(@Nullable String login);

}
