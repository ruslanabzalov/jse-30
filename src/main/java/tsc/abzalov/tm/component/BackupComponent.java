package tsc.abzalov.tm.component;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.SECONDS;

public final class BackupComponent {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public BackupComponent(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void init() {
        loadBackup();
        scheduledBackupSave();
    }

    private void loadBackup() {
        executeBackupLoadCommand();
    }

    private void scheduledBackupSave() {
        val initialDelay = 1;
        val delay = 30;
        executorService.scheduleWithFixedDelay(this::executeBackupSaveCommand, initialDelay, delay, SECONDS);
    }

    private void executeBackupLoadCommand() {
        @NotNull val backupLoadCommandName = "backup-load";
        executeCommand(backupLoadCommandName);
    }

    private void executeBackupSaveCommand() {
        @NotNull val backupSaveCommandName = "backup-save";
        executeCommand(backupSaveCommandName);
    }

    private void executeCommand(@NotNull final String commandName) {
        @NotNull val commandService = serviceLocator.getCommandService();
        @NotNull val command = commandService.getCommandByName(commandName);
        command.execute();
    }

}
