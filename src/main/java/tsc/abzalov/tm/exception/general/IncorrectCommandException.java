package tsc.abzalov.tm.exception.general;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException() {
        super("Command is not available!\nPlease, use \"help\" command.");
    }

    public IncorrectCommandException(@NotNull final String command) {
        super("Command \"" + command + "\" is not available!\nPlease, use \"help\" command.");
    }

}
