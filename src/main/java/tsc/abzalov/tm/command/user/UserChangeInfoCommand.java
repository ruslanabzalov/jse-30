package tsc.abzalov.tm.command.user;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputFirstName;
import static tsc.abzalov.tm.util.InputUtil.inputLastName;

@SuppressWarnings("unused")
public final class UserChangeInfoCommand extends AbstractCommand {

    public UserChangeInfoCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "change-user-info";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change user info.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("CHANGE USER INFO");
        @NotNull val userFirstName = inputFirstName();
        @Nullable val userLastName = inputLastName();
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val authService = serviceLocator.getAuthService();
        @Nullable val currentUserId = authService.getCurrentUserId();
        @NotNull val userService = serviceLocator.getUserService();

        @Nullable val updatedUser = userService.editUserInfoById(currentUserId, userFirstName, userLastName);
        Optional.ofNullable(updatedUser).orElseThrow(() -> new UserIsNotExistException(currentUserId));

        System.out.println("User info successfully changed.\n");
    }

}
