package tsc.abzalov.tm.command.user;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;

@SuppressWarnings("unused")
public final class UserShowInfoCommand extends AbstractCommand {

    public UserShowInfoCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-user-info";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all user information.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("SHOW USER INFO");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();
        @NotNull val userService = serviceLocator.getUserService();

        @Nullable val searchedUser = userService.findById(currentUserId);
        System.out.println(searchedUser + "\n");
    }

}
