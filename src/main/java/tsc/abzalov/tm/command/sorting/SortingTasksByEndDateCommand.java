package tsc.abzalov.tm.command.sorting;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

@SuppressWarnings("unused")
public final class SortingTasksByEndDateCommand extends AbstractCommand {

    public SortingTasksByEndDateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-tasks-by-end-date";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort tasks by end date.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL TASKS LIST SORTED BY END DATE");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull val tasks = taskService.sortByEndDate(currentUserId);
            var taskIndex = 0;
            for (@NotNull val task : tasks) {
                taskIndex = taskService.indexOf(currentUserId, task) + 1;
                System.out.println(taskIndex + ". " + task);
            }

            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
