package tsc.abzalov.tm.command.auth;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputLogin;
import static tsc.abzalov.tm.util.InputUtil.inputPassword;

@SuppressWarnings("unused")
public final class AuthLoginCommand extends AbstractCommand {

    public AuthLoginCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "login";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Login as existing user.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("LOGIN");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val login = inputLogin();
        @NotNull val password = inputPassword();
        authService.login(login, password);
        System.out.println("Successful login.\n");
    }

}
