package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.entity.AbstractEntityDeleteByIndexCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

@SuppressWarnings("unused")
public final class ProjectDeleteByIndexCommand extends AbstractEntityDeleteByIndexCommand<Project> {

    public ProjectDeleteByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator, Project.class);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

}
