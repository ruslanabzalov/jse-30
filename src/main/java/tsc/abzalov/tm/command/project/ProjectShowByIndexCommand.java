package tsc.abzalov.tm.command.project;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

@SuppressWarnings("unused")
public final class ProjectShowByIndexCommand extends AbstractCommand {

    public ProjectShowByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-project-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND PROJECT BY INDEX");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            val projectIndex = inputIndex();
            System.out.println();

            @Nullable val searchedProject = projectService.findByIndex(currentUserId, projectIndex);
            val isProjectExist = Optional.ofNullable(searchedProject).isPresent();
            if (isProjectExist) {
                val projectOutputIndex = projectService.indexOf(currentUserId, searchedProject) + 1;
                System.out.println(projectOutputIndex + ". " + searchedProject + "\n");
                return;
            }

            System.out.println("Searched project was not found.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
