package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.entity.AbstractEntityDeleteAllCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

@SuppressWarnings("unused")
public final class ProjectDeleteAllCommand extends AbstractEntityDeleteAllCommand<Project> {

    public ProjectDeleteAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator, Project.class);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-all-projects";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete all projects.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

}
