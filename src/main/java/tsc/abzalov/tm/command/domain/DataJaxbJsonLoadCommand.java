package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import javax.xml.bind.JAXBContext;
import java.io.File;
import java.io.FileNotFoundException;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataJaxbJsonLoadCommand extends AbstractDomainCommand {

    public DataJaxbJsonLoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-jaxb-json-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from JSON format via JAXB.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty(JAXB_CONTEXT_FACTORY_PROPERTY_NAME, JAXB_CONTEXT_FACTORY_PROPERTY_VALUE);

        @NotNull val file = new File(JAXB_JSON_FILENAME);
        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val unmarshaller = context.createUnmarshaller();

        unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, JAXB_MEDIA_TYPE);
        unmarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        @Nullable val domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);

        System.out.println("Data was loaded from JSON format via JAXB.\nPlease, re-login.\n");
    }

}
