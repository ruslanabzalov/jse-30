package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import javax.xml.bind.JAXBContext;
import java.io.File;
import java.io.FileNotFoundException;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataJaxbXmlLoadCommand extends AbstractDomainCommand {

    public DataJaxbXmlLoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-jaxb-xml-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from XML format via JAXB.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val file = new File(JAXB_XML_FILENAME);
        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val context = JAXBContext.newInstance(Domain.class);
        @NotNull val unmarshaller = context.createUnmarshaller();

        @Nullable val domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);

        System.out.println("Data was loaded from XML format via JAXB.\nPlease, re-login.\n");
    }

}
