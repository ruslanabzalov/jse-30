package tsc.abzalov.tm.command.entity;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

public abstract class AbstractEntityShowAllCommand<E extends AbstractBusinessEntity> extends AbstractEntityCommand<E> {

    public AbstractEntityShowAllCommand(@NotNull final IServiceLocator serviceLocator,
                                        @NotNull final Class<E> clazz) {
        super(serviceLocator, null, clazz);
    }
}
