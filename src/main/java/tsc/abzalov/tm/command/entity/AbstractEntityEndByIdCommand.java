package tsc.abzalov.tm.command.entity;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.Optional;

import static tsc.abzalov.tm.util.InputUtil.inputId;

public abstract class AbstractEntityEndByIdCommand<E extends AbstractBusinessEntity> extends AbstractEntityCommand<E> {

    public AbstractEntityEndByIdCommand(@NotNull IServiceLocator serviceLocator,
                                        @NotNull final Class<E> clazz) {
        super(serviceLocator, null, clazz);
    }

    @Override
    public void execute() {
        System.out.println("END ENTITY BY ID");

        var areEntitiesExist = false;
        if (getTypeName().equals(Project.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getProjectService().size(getAuthService().getCurrentUserId()) != 0;
        if (getTypeName().equals(Task.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getTaskService().size(getAuthService().getCurrentUserId()) != 0;

        if (areEntitiesExist) {
            @Nullable AbstractBusinessEntity entity = null;
            if (getTypeName().equals(Project.class.getCanonicalName()))
                entity = getServiceLocator().getProjectService().endById(getAuthService().getCurrentUserId(), inputId());
            if (getTypeName().equals(Task.class.getCanonicalName()))
                entity = getServiceLocator().getProjectService().endById(getAuthService().getCurrentUserId(), inputId());

            val isProjectExist = Optional.ofNullable(entity).isPresent();
            if (isProjectExist) {
                System.out.println("Entity was successfully ended.\n");
                return;
            }

            System.out.println("Entity was not ended! Please, check that entity exists or it has correct status.\n");
            return;
        }

        System.out.println("Entities are not exist.\n");
    }

}
