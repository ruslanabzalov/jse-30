package tsc.abzalov.tm.command.entity;

import lombok.var;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

public abstract class AbstractEntityDeleteAllCommand<E extends AbstractBusinessEntity>
        extends AbstractEntityCommand<E> {

    public AbstractEntityDeleteAllCommand(@NotNull final IServiceLocator serviceLocator,
                                          @NotNull final Class<E> typeClass) {
        super(serviceLocator, null, typeClass);
    }

    @Override
    public void execute() {
        System.out.println("ENTITY DELETION");

        var areEntitiesExist = false;
        if (getTypeName().equals(Project.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getProjectService().size(getAuthService().getCurrentUserId()) != 0;
        if (getTypeName().equals(Task.class.getCanonicalName()))
            areEntitiesExist = getServiceLocator().getTaskService().size(getAuthService().getCurrentUserId()) != 0;

        if (areEntitiesExist) {
            if (getTypeName().equals(Project.class.getCanonicalName()))
                getServiceLocator().getProjectService().clear(getAuthService().getCurrentUserId());
            if (getTypeName().equals(Task.class.getCanonicalName()))
                getServiceLocator().getTaskService().clear(getAuthService().getCurrentUserId());
            System.out.println("Entities were deleted.\n");
            return;
        }

        System.out.println("Entities are not exist.\n");
    }

}
