package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.entity.AbstractEntityCreateCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

@SuppressWarnings("unused")
public final class TaskCreateCommand extends AbstractEntityCreateCommand<Task> {

    public TaskCreateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator, Task::new, Task.class);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "create-task";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create task.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

}
