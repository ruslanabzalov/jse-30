package tsc.abzalov.tm.command.task;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;

@SuppressWarnings("unused")
public final class TaskShowByNameCommand extends AbstractCommand {

    public TaskShowByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-task-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("FIND TASK BY NAME");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull val taskName = inputName();
            System.out.println();

            @Nullable val task = taskService.findByName(currentUserId, taskName);
            val wasTaskFound = Optional.ofNullable(task).isPresent();
            if (wasTaskFound) {
                val taskIndex = taskService.indexOf(currentUserId, task) + 1;
                System.out.println(taskIndex + ". " + task + "\n");
                return;
            }

            System.out.println("Searched task was not found.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
