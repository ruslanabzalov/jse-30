package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.entity.AbstractEntityDeleteAllCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

@SuppressWarnings("unused")
public final class TaskDeleteAllCommand extends AbstractEntityDeleteAllCommand<Task> {

    public TaskDeleteAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator, Task.class);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-all-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete all tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

}
